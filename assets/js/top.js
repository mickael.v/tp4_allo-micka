/****************************************************************************
 *                        Mes variables                                     *
 ****************************************************************************/
const apiKey = '';
let urlPoster300px =`https://image.tmdb.org/t/p/w300`;
let result;


/****************************************************************************
 *               La base de données TMDB fournit le top du moment           *
 *                    actualisation chaque jour                             *
 *                                                                          *
 *               Le principe d'affichage des resultas rerend le principe    *
 *               de la recherche de film    (apps.js)                       * 
 *                                                                          *
 ****************************************************************************/
function fetchTop() {
    fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=fr-FR&page=1`)
      .then((response) => response.json())
      .then((json) => {
        if (json.total_results==0) {
          let noresult = document.createElement('p')
          noresult.innerHTML= 'Aucun résultat trouvé';
          result.append(noresult)
          return
        }
        for (let i = 0; i < json.results.length; i++) {
      

            let id=json.results[i].id;
            
      
            let input=document.getElementById("maDiv");
      
            let div=document.createElement("div");
            div.setAttribute ("class","card");
            div.setAttribute ("style","width: 10rem;");
            
            
      
            
            let poster=document.createElement("img"); //creer la balise img
            let urlPoster= json.results[i].poster_path; // recuperer l'url du poster sur la BD
            poster.setAttribute ("src",`${urlPoster300px}${urlPoster}`); // associer l url de base plus l'url du json pour
            poster.setAttribute ("ALT","affiche du film ");
            div.appendChild(poster);
      
      
            
        
            let cardBody =  document.createElement("div");
            cardBody.setAttribute ("class","card-body");
            cardBody.setAttribute ("id","card-body"+i);
            cardBody.setAttribute ("data-id", `${id}`)
            let urlPageFilm = window.location.protocol+"///home/mvandenbulcke/tp4_allo-micka/"+"pagefilm.html";
            
            let urlFinalPageFilm= new URL(urlPageFilm);
            urlFinalPageFilm.searchParams.set("movieID",id)
            cardBody.innerHTML=`<a href=${urlFinalPageFilm}>`+json.results[i].title+"</a>"
            div.appendChild(cardBody);
           
            input.appendChild(div);
            }

    })
}

fetchTop ();
