/****************************************************************************
 *                        Mes variables                                     *
 ****************************************************************************/
const apiKey = '';
let urlPoster185px =`https://image.tmdb.org/t/p/w185`;
let result;

/****************************************************************************
 *                       Recuperation de l'id de la serie transmise dans l'url  *
 *                                                                          *
 ****************************************************************************/
var mysearch = window.location.search;
const myUrlParams = new URLSearchParams(mysearch);
let id= myUrlParams.get ("movieID");
     
/***********************************************************************************************
 *                  Afficher les infos de la serie                                             *
 *                                                                                             *
 *                Fonction AfficherFilm : a l'ai de l'id recuperer dans l'url                  *
 *                je vais rechercher le film dans la base de données TMDB et                   *
 *                j'en affiche les informations...                                             *
 *                ici j'ai choisit d(afficher :                                                *
 *                                               - L'affiche                                   *
 *                                               - Le synopsis (overview)                      *
 *                                               - La date de réalisation                      *
 *                                               - La note spectateurs sur 10                  *
 *                                                                                             *
 *                                                                                             *
 ************************************************************************************************/
     function afficherSerie () {
          fetch(`https://api.themoviedb.org/3/tv/${id}?api_key=${apiKey}&language=fr-FR`)
            .then((response) => response.json())
            .then((json) => {
               if (json.total_results==0) {
                    let noresult = document.createElement('p')
                    noresult.innerHTML= 'Aucun résultat trouvé';
                    result.append(noresult)
                    return
               }
               console.log (json)
              
               let maSerie =document.getElementById("maDiv");
               document.createElement("div");
               let poster=document.createElement("img"); 
               let urlPoster= json.backdrop_path; 
               poster.setAttribute ("src",`${urlPoster185px}${urlPoster}`); 
               poster.setAttribute ("ALT","affiche du film ");
               maSerie.appendChild(poster);

               let monTitle= document.getElementById("title")
               let divTitle=document.createElement("h1");
               divTitle.innerHTML= json.name;
               monTitle.appendChild(divTitle)

               let monContenu= document.getElementById("contenu");
               let pOverview=document.createElement("P");
               pOverview.innerHTML= "Synopsis: "+ json.overview;
               monContenu.appendChild(pOverview);

               let pDate=document.createElement("P");
               pDate.innerHTML= "Date de la première diffusion :"+ json.first_air_date;
               monContenu.appendChild(pDate);

               let pNote=document.createElement("P");
               pNote.innerHTML= "Note des spectateurs sur 10 :"+ json.vote_average;
               monContenu.appendChild(pNote);

            }



)}

afficherSerie();
