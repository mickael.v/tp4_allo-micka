/****************************************************************************
 *                        Mes variables                                     *
 ****************************************************************************/

const apiKey = '';
let Btnrechercher = document.getElementById('btnRechercher');
let maRecherche = document.getElementById('inputText');
let urlPoster185px =`https://image.tmdb.org/t/p/w185`;
let id;
let result;
let page = 1;
let btnUp= document.getElementById('up');
let btnDown= document.getElementById('down');



/****************************************************************************
 *        ecoute le bouton de recherche et lancer la recherche au click     *
 *                                                                          *
 *  -L26: refresh() => appel ma fonction qui rafraichit la page a           *
 *        chaque nouvelle recherche                                         *
 *  -L27: fetchfilm => appel ma fonction de recherche de film               *    
*****************************************************************************/


Btnrechercher.addEventListener('click', () => {
  refresh();
  fetchfilms(maRecherche.value)
  visible();
})

/*****************************************************************************
 *                  Pagination                                               *
 *                                                                           *
 *       -Au click sur le bouton up ou down, je change la valeur             *
 *        de la variable page qui est contenu dans l'url du fetch            *
 *       - j'utlise un eventListener sur le click bouton pour appeler        *
 *          la funtion up ou down                                            *
 *****************************************************************************/
btnDown.addEventListener('click', () =>{
  refresh();
  paginationDown();
  
  
})

btnUp.addEventListener('click', () =>{
  refresh();
  paginationUp();
  
  
})

function paginationDown (){
page = page-1
if (page<=1){
  page=1
}
fetchfilms(maRecherche.value);
}

function paginationUp (){
  page = page+1
  fetchfilms (maRecherche.value);
  }

/**********************************************************************************************
 *                             Fonction recherche de film ( fetch =>api tmdb)                 *
 *                                                                                            *
 *                                                                                            * 
 *        -L89 : Enregistrement de l'id pour chaque resultat                                  *
 *        -L93 à L95 : creation dynamique des cards bootstrap                                 *
 *        -L98 à L102 : insertion du poster de film pour chaque card/resultat                 *
 *        -L106 à L110: creation d'une nouvelle card par resultat trouvé                      *
 *        -L111 : mettre a la fin de l'url film, son id pour le recuperer sur la page du film *
 *        -L114 à 117: inserer dans la card le tire du film en mode clickable                 *
 *                                                                                            *
 **********************************************************************************************/ 

function fetchfilms(search) {
  fetch(`https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&language=fr-FR&query=${search}&page=${page}&include_adult=false`)
    .then((response) => response.json()) 
    .then((json) => {                    
      if (json.total_results==0) {
        let noresult = document.createElement('p')
        noresult.innerHTML= 'Aucun résultat trouvé';
        result.append(noresult)
        return
      }

    for (let i = 0; i < json.results.length; i++) {
      

      id=json.results[i].id; 
      
      
      let input=document.getElementById("maDiv");
      let div=document.createElement("div");
      div.setAttribute ("class","card");
      div.setAttribute ("style","width: 10rem;");
      
      
      let poster=document.createElement("img"); 
      let urlPoster= json.results[i].poster_path; 
      poster.setAttribute ("src",`${urlPoster185px}${urlPoster}`); 
      poster.setAttribute ("ALT","affiche du film ");
      div.appendChild(poster);

      
      let cardBody =  document.createElement("div");
      cardBody.setAttribute ("class","card-body");
      cardBody.setAttribute ("id","card-body"+i);
      cardBody.setAttribute ("data-id", `${id}`)
      let urlPageFilm = window.location.protocol+"///home/mvandenbulcke/tp4_allo-micka/"+"pagefilm.html";
      
      let urlFinalPageFilm= new URL(urlPageFilm);
      urlFinalPageFilm.searchParams.set("movieID",id) 
      cardBody.innerHTML=`<a href=${urlFinalPageFilm}>`+json.results[i].title+"</a>"
      div.appendChild(cardBody);
  
      input.appendChild(div);
      }
    })

  }

  /**********************************************************************************************
   *                  Fonction de rafraichissement de la page a chaque nouvelle recherche       *
   *                                                                                            *
   **********************************************************************************************/
  function refresh(){
    let refresh=document.getElementById("maDiv");
    for (let i = refresh.children.length-1; i >= 0 ; i--) {
      refresh.removeChild(refresh.children[i])
    
    }
  }

/************************************************************************************************
 *                      Animation du titre en mode star wars                                    *
 *                                                                                              *
 *        module trouver en open source sur internet à l'adresse :                              *
 *          https://la-cascade.io/css-animation-star-wars/                                      *
 *                                                                                              *
 *         création des svg specifique au TP : Rose Vandenbulcke / Littlerphotographie          *
 *                                                                                              *
 *                                                                                              *
 ************************************************************************************************/


let byline = document.getElementById('byline');     // Find the H2
let bylineText = byline.innerHTML;                                      // Get the content of the H2
let bylineArr = bylineText.split('');                                   // Split content into array
byline.innerHTML = '';                                                      // Empty current content

let span;                   // Create variables to create elements
let yletter;

for(let i=0;i<bylineArr.length;i++){                                    // Loop for every letter
  span = document.createElement("span");                    // Create a <span> element
  let letter = document.createTextNode(bylineArr[i]);   // Create the letter
  if(bylineArr[i] == ' ') {                                             // If the letter is a space...
    byline.appendChild(letter);                 // ...Add the space without a span
  } else {
        span.appendChild(letter);                       // Add the letter to the span
    byline.appendChild(span);                   // Add the span to the h2
  }
}
  
/****************************************************************************
 *    Rendre visible les boutons de paginations apres un resultat           *
 *                                                                          *
 ***************************************************************************/
function visible (){
  let btnDownvisible = document.getElementById ("down");
  let btnUpvisible = document.getElementById ("up");
  btnDownvisible.classList.remove ("hidden");
  btnDownvisible.classList.add ("visible");
  btnUpvisible.classList.remove ("hidden");
  btnUpvisible.classList.add ("visible");
}

  
