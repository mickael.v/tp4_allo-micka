/****************************************************************************
 *                        Mes variables                                     *
 ****************************************************************************/
const apiKey = '';
let Btnrechercher = document.getElementById('btnRechercher');
let maRecherche = document.getElementById('inputText');
let urlPoster185px =`https://image.tmdb.org/t/p/w185`;
let id;
let result;
let page = 1;
let btnUp= document.getElementById('up');
let btnDown= document.getElementById('down');


/****************************************************************************
 *        ecoute le bouton de recherche et lancer la recherche au click     *
 *                                                                          *
 *  -L23: refresh() => appel ma fonction qui rafraichit la page a           *
 *        chaque nouvelle recherche                                         *
 *  -L24: fetchseries => appel ma fonction de recherche de series             *    
*****************************************************************************/
Btnrechercher.addEventListener('click', e => {
  refresh();
  fetchseries(maRecherche.value)
  visible();
})

/*****************************************************************************
 *                  Pagination                                               *
 *                                                                           *
 *       -Au click sur le bouton up ou down, je change la valeur             *
 *        de la variable page qui est contenu dans l'url du fetch            *
 *       - j'utlise un eventListener sur le click bouton pour appeler        *
 *          la funtion up ou down                                            *
 *****************************************************************************/
btnDown.addEventListener('click', e=>{
  refresh();
  paginationDown();
  
})

btnUp.addEventListener('click', e=>{
  refresh();
  paginationUp();
  
})

function paginationDown (){
page = page-1
if (page<=1){
  page=1
}
fetchseries(maRecherche.value);
}

function paginationUp (){
  page = page+1
  fetchseries (maRecherche.value);
  }
/**********************************************************************************************
 *                             Fonction recherche de serie ( fetch =>api tmdb)                *
 *                                                                                            *
 *                                                                                            * 
 =                            Meme principe que recherche de films  (app.js)                  *
 *                                                                                            *
 **********************************************************************************************/
function fetchseries(search) {
  fetch(`https://api.themoviedb.org/3/search/tv?api_key=${apiKey}&language=fr-FR&page=${page}&query=${search}&include_adult=false`)
    .then((response) => response.json())
    .then((json) => {
      if (json.total_results==0) {
        let noresult = document.createElement('p')
        noresult.innerHTML= 'Aucun résultat trouvé';
        result.append(noresult)
        return
      }

    for (let i = 0; i < json.results.length; i++) {
      

      id=json.results[i].id;
      console.log (id)

      let input=document.getElementById("maDiv");

      let div=document.createElement("div");
      div.setAttribute ("class","card");
      div.setAttribute ("style","width: 10rem;");
      
      

      
      let poster=document.createElement("img"); //creer la balise img
      let urlPoster= json.results[i].poster_path; // recuperer l'url du poster sur la BD
      poster.setAttribute ("src",`${urlPoster185px}${urlPoster}`); // associer l url de base plus l'url du json pour
      poster.setAttribute ("ALT","affiche de la serie");
      div.appendChild(poster);


      
  
      let cardBody =  document.createElement("div");
      cardBody.setAttribute ("class","card-body");
      cardBody.setAttribute ("id","card-body"+i);
      cardBody.setAttribute ("data-id", `${id}`)
      let urlPageSerie = window.location.protocol+"///home/mvandenbulcke/tp4_allo-micka/"+"pageSeries.html";
      
      let urlFinalPageSerie= new URL(urlPageSerie);
      urlFinalPageSerie.searchParams.set("movieID",id)
      cardBody.innerHTML=`<a href=${urlFinalPageSerie}>`+json.results[i].name+"</a>"
      div.appendChild(cardBody);
     
      
      
    

      input.appendChild(div);
      }
    })

  }
  /**********************************************************************************************
   *                  Fonction de rafraichissement de la page a chaque nouvelle recherche       *
   *                                                                                            *
   **********************************************************************************************/
  function refresh(){
    let refresh=document.getElementById("maDiv");
    for (let i = refresh.children.length-1; i >= 0 ; i--) {
      refresh.removeChild(refresh.children[i])
    
    }
  }
/****************************************************************************
 *    Rendre visible les boutons de paginations apres un resultat           *
 *                                                                          *
 ****************************************************************************/
  function visible (){
    let btnDownvisible = document.getElementById ("down");
    let btnUpvisible = document.getElementById ("up");
    btnDownvisible.classList.remove ("hidden");
    btnDownvisible.classList.add ("visible");
    btnUpvisible.classList.remove ("hidden");
    btnUpvisible.classList.add ("visible");
  }
